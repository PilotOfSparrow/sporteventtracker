package com.example.sporteventtracker.ui.fragments.add_event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.sporteventtracker.R
import com.example.sporteventtracker.extensions.getViewModel
import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.models.EventStorageType
import com.example.sporteventtracker.viewmodel.AddEventViewModel
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import kotlinx.android.synthetic.main.fragment_add_event.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.*

class AddEventFragment : Fragment() {

    companion object {
        fun newInstance() = AddEventFragment()
    }

    private val nameMaxLength by lazy(LazyThreadSafetyMode.NONE) {
        requireContext().resources.getInteger(R.integer.event_name_max_length)
    }

    private val locationMaxLength by lazy(LazyThreadSafetyMode.NONE) {
        requireContext().resources.getInteger(R.integer.event_location_max_length)
    }

    private val dateTimeFormat = DateTimeFormat.longDateTime()

    private lateinit var viewModel: AddEventViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_add_event, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = getViewModel(AddEventViewModel::class.java)
        viewModel.savingResultLiveData.observe(this, Observer { savingSucceed ->
            addEventProgress.hide()
            if (!savingSucceed) {
                showErrorDialog(R.string.add_event_error_saving)
            }
        })

        setupTimePickerButton(
            button = addEventSetTimeEndButton,
            editText = addEventTimeEndInputLayout.editText!!,
            pickerTitleRes = R.string.add_event_hint_time_end
        )
        setupTimePickerButton(
            button = addEventSetTimeStartButton,
            editText = addEventTimeStartInputLayout.editText!!,
            pickerTitleRes = R.string.add_event_hint_time_start
        )
        addEventButton.setOnClickListener {
            trySaveEvent()
        }
    }

    private fun setupTimePickerButton(
        button: View,
        editText: EditText,
        @StringRes pickerTitleRes: Int
    ) {
        button.setOnClickListener {
            showDateTimePicker(pickerTitleRes) { date ->
                editText.setText(dateTimeFormat.print(DateTime(date)))
            }
        }
    }

    private fun showDateTimePicker(
        @StringRes titleRes: Int,
        onDateSelectedAction: (date: Date) -> Unit
    ) {
        SingleDateAndTimePickerDialog.Builder(requireContext())
            .listener(onDateSelectedAction)
            .title(getString(titleRes))
            .build()
            .display()
    }

    private fun showErrorDialog(@StringRes message: Int) {
        AlertDialog.Builder(requireContext())
            .setMessage(message)
            .setPositiveButton(R.string.global_ok) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun trySaveEvent() {
        val name = addEventNameEditText.text?.toString().orEmpty()
        val location = addEventLocationEditText.text?.toString().orEmpty()
        val timeEnd = addEventTimeEndInputLayout.editText?.textToDateTime()
        val timeStart = addEventTimeStartInputLayout.editText?.textToDateTime()
        val storageTypeViewId = addEventStorageSelectionChipGroup.checkedChipId

        val errorRes = when {
            name.isBlank() || name.length > nameMaxLength -> R.string.add_event_error_illegal_name
            location.isBlank() || location.length > locationMaxLength -> R.string.add_event_error_illegal_location
            timeStart == null -> R.string.add_event_error_illegal_time_start
            timeEnd == null -> R.string.add_event_error_illegal_time_end
            timeStart > timeEnd -> R.string.add_event_error_illegal_time_period
            storageTypeViewId == View.NO_ID -> R.string.add_event_error_illegal_storage
            else -> null
        }

        errorRes?.let(::showErrorDialog) ?: run {
            addEventProgress.show()
            viewModel.saveEvent(
                EventFields(
                    name = name,
                    location = location,
                    timeEnd = timeEnd!!.millis,
                    timeStart = timeStart!!.millis,
                    storageType = when (storageTypeViewId) {
                        R.id.addEventStorageTypeLocal -> EventStorageType.LOCAL
                        R.id.addEventStorageTypeFirebase -> EventStorageType.FIREBASE
                        else -> throw IllegalStateException(
                            "Selected id: $storageTypeViewId not associated with storage type"
                        )
                    }
                )
            )
        }
    }

    private fun EditText.textToDateTime() =
        text?.toString()?.takeIf(String::isNotBlank)?.let(dateTimeFormat::parseDateTime)

}
