package com.example.sporteventtracker.ui.fragments.show_events

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.sporteventtracker.R
import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.models.EventStorageType
import kotlinx.android.synthetic.main.item_event.view.*
import org.joda.time.format.DateTimeFormat

class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val dateTimeFormat = DateTimeFormat.longDateTime()

    fun bind(event: EventFields) {
        itemView.itemEventName.text = event.name
        itemView.itemEventLocation.text = event.location
        itemView.itemEventTimeEnd.text = dateTimeFormat.print(event.timeEnd)
        itemView.itemEventTimeStart.text = dateTimeFormat.print(event.timeStart)

        itemView.setBackgroundColor(
            ContextCompat.getColor(
                itemView.context,
                if (event.storageType == EventStorageType.LOCAL) {
                    R.color.colorPrimaryDark
                } else {
                    android.R.color.background_dark
                }
            )
        )
    }

}
