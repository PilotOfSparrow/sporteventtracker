package com.example.sporteventtracker.ui.fragments.show_events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.sporteventtracker.R
import com.example.sporteventtracker.extensions.getViewModel
import com.example.sporteventtracker.viewmodel.ShowEventsViewModel
import kotlinx.android.synthetic.main.fragment_show_events.*

class ShowEventsFragment : Fragment() {

    companion object {
        fun newInstance() = ShowEventsFragment()
    }

    private lateinit var viewModel: ShowEventsViewModel

    private val eventAdapter = EventAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_show_events, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showEventsRecyclerView.adapter = eventAdapter

        viewModel = getViewModel(ShowEventsViewModel::class.java)
        viewModel.eventsLiveData.observe(this, Observer { events ->
            showEventsSwipeRefresh.isRefreshing = false
            eventAdapter.submitList(events)
        })

        showEventsSwipeRefresh.setOnRefreshListener(viewModel::fetchEvents)
    }

}
