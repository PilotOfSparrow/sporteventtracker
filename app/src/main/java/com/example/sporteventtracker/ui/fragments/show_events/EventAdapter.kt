package com.example.sporteventtracker.ui.fragments.show_events

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.*
import com.example.sporteventtracker.R
import com.example.sporteventtracker.model.models.EventFields

class EventAdapter : RecyclerView.Adapter<EventViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<EventFields>() {
        override fun areItemsTheSame(oldItem: EventFields, newItem: EventFields): Boolean =
            areContentsTheSame(oldItem, newItem)

        override fun areContentsTheSame(oldItem: EventFields, newItem: EventFields): Boolean =
            oldItem == newItem
    }

    private val differ = AsyncListDiffer(
        AdapterListUpdateCallback(this),
        AsyncDifferConfig.Builder<EventFields>(diffCallback).build()
    )

    fun submitList(list: List<EventFields>) = differ.submitList(list)

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder =
        EventViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_event,
                parent,
                false
            )
        )

}
