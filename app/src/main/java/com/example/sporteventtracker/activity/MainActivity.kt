package com.example.sporteventtracker.activity

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.sporteventtracker.R
import com.example.sporteventtracker.ui.fragments.add_event.AddEventFragment
import com.example.sporteventtracker.ui.fragments.show_events.ShowEventsFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val ADD_EVENT_TAB_POSITION = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        mainActivityPager.adapter = object : FragmentPagerAdapter(supportFragmentManager) {
            override fun getCount(): Int = 2

            override fun getItem(position: Int): Fragment = when (position) {
                ADD_EVENT_TAB_POSITION -> AddEventFragment.newInstance()
                else -> ShowEventsFragment.newInstance()
            }

            override fun getPageTitle(position: Int): CharSequence = getString(
                when (position) {
                    ADD_EVENT_TAB_POSITION -> R.string.tab_add
                    else -> R.string.tab_show
                }
            )
        }

        mainActivityPager.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(mainActivityTabLayout)
        )
        mainActivityPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                val inputManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                currentFocus?.windowToken?.let { token ->
                    inputManager.hideSoftInputFromWindow(token, 0)
                }
            }
        })
        mainActivityTabLayout.addOnTabSelectedListener(
            TabLayout.ViewPagerOnTabSelectedListener(mainActivityPager)
        )
    }

}
