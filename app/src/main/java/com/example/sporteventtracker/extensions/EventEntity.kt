package com.example.sporteventtracker.extensions

import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.models.EventStorageType
import com.example.sporteventtracker.model.persistence.event.EventEntity

fun EventEntity.toEventFields() = EventFields(
    name = name,
    location = location,
    timeEnd = timeEnd,
    timeStart = timeStart,
    storageType = EventStorageType.LOCAL // Entity used only in database
)
