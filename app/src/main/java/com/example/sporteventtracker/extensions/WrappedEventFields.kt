package com.example.sporteventtracker.extensions

import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.models.EventStorageType
import com.example.sporteventtracker.model.models.WrappedEventFields

fun WrappedEventFields.unwrapToEventFields() = EventFields(
    name = name.stringValue,
    location = location.stringValue,
    timeEnd = timeEnd.integerValue,
    timeStart = timeStart.integerValue,
    storageType = EventStorageType.FIREBASE // Unwrapping needed only if source is firebase
)
