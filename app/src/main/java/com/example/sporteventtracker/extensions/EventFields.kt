package com.example.sporteventtracker.extensions

import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.models.EventFirebaseDocument
import com.example.sporteventtracker.model.models.WrappedEventFields
import com.example.sporteventtracker.model.models.WrappedValue
import com.example.sporteventtracker.model.persistence.event.EventEntity

fun EventFields.wrapToFirebaseDocument() = EventFirebaseDocument(
    fields = WrappedEventFields(
        name = WrappedValue.StringValue(name),
        location = WrappedValue.StringValue(location),
        timeEnd = WrappedValue.IntegerValue(timeEnd),
        timeStart = WrappedValue.IntegerValue(timeStart)
    )
)

fun EventFields.toEventEntity() = EventEntity(
    name = name,
    location = location,
    timeEnd = timeEnd,
    timeStart = timeStart
)
