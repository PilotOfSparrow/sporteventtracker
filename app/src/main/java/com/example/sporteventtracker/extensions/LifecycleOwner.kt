package com.example.sporteventtracker.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.sporteventtracker.SportsEventTrackingApp

fun <T : ViewModel> LifecycleOwner.getViewModel(modelClass: Class<T>): T =
    when (this) {
        is Fragment -> {
            ViewModelProviders.of(
                this,
                (activity!!.application as SportsEventTrackingApp).viewModelFactory
            )
        }
        is FragmentActivity -> {
            ViewModelProviders.of(
                this,
                (application as SportsEventTrackingApp).viewModelFactory
            )
        }
        else -> {
            throw IllegalArgumentException("Not supported lifecycleOwner: $this")
        }
    }.get(modelClass)
