package com.example.sporteventtracker.model.models

import kotlinx.serialization.Serializable

interface FirebaseDocumentList<T : FirebaseDocument<*>> {
    val documents: List<T>
}

@Serializable
data class EventsFirebaseDocumentList(
    override val documents: List<EventFirebaseDocument>
) : FirebaseDocumentList<EventFirebaseDocument>
