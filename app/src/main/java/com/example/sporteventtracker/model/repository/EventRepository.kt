package com.example.sporteventtracker.model.repository

import com.example.sporteventtracker.extensions.toEventEntity
import com.example.sporteventtracker.extensions.toEventFields
import com.example.sporteventtracker.extensions.unwrapToEventFields
import com.example.sporteventtracker.extensions.wrapToFirebaseDocument
import com.example.sporteventtracker.model.api.EventApi
import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.models.EventFirebaseDocument
import com.example.sporteventtracker.model.models.EventStorageType
import com.example.sporteventtracker.model.models.FirebaseDocumentList
import com.example.sporteventtracker.model.persistence.event.EventDao
import com.example.sporteventtracker.model.persistence.event.EventEntity
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EventRepository @Inject constructor(
    private val eventApi: EventApi,
    private val eventDao: EventDao
) {

    fun getEvents(): Single<List<EventFields>> =
        Single.zip(
            getDatabaseEvents(),
            getFirebaseEvents(),
            BiFunction { databaseEvents, firebaseEvents -> databaseEvents + firebaseEvents }
        )

    fun saveEvent(eventFields: EventFields): Single<Boolean> = when (eventFields.storageType) {
        EventStorageType.LOCAL -> saveToDatabase(eventFields)
        EventStorageType.FIREBASE -> saveToFirebase(eventFields)
    }

    private fun saveToFirebase(eventFields: EventFields): Single<Boolean> =
        eventApi
            .saveEvent(eventFields.wrapToFirebaseDocument())
            .map { response -> !response.name.isNullOrBlank() }

    private fun saveToDatabase(eventFields: EventFields): Single<Boolean> =
        Completable
            .fromAction {
                eventDao.insert(eventFields.toEventEntity())
            }
            .toSingle { true }
            .subscribeOn(Schedulers.io())

    private fun getDatabaseEvents(): Single<List<EventFields>> =
        eventDao
            .getEventsSingle()
            .map { eventEntities -> eventEntities.map(EventEntity::toEventFields) }
            .subscribeOn(Schedulers.io())

    private fun getFirebaseEvents(): Single<List<EventFields>> =
        eventApi
            .getEvents()
            .map(FirebaseDocumentList<EventFirebaseDocument>::documents)
            .map { eventDocuments -> eventDocuments.map { it.fields.unwrapToEventFields() } }

}
