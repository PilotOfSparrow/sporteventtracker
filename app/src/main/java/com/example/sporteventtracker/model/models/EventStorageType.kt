package com.example.sporteventtracker.model.models

enum class EventStorageType {
    LOCAL,
    FIREBASE
}
