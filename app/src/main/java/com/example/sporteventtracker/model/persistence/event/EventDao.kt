package com.example.sporteventtracker.model.persistence.event

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
abstract class EventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(eventEntity: EventEntity)

    @Query("SELECT * FROM events")
    abstract fun getEventsSingle(): Single<List<EventEntity>>

}
