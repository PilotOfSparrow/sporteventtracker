package com.example.sporteventtracker.model.models

import kotlinx.serialization.Serializable

data class EventFields(
    val name: String,
    val location: String,
    val timeEnd: Long,
    val timeStart: Long,
    val storageType: EventStorageType
)

@Serializable
data class WrappedEventFields(
    val name: WrappedValue.StringValue,
    val location: WrappedValue.StringValue,
    val timeEnd: WrappedValue.IntegerValue,
    val timeStart: WrappedValue.IntegerValue
)
