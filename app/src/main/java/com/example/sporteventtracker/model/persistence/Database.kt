package com.example.sporteventtracker.model.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.sporteventtracker.model.persistence.event.EventDao
import com.example.sporteventtracker.model.persistence.event.EventEntity

@Database(
    version = 1,
    entities = [
        EventEntity::class
    ]
)
abstract class Database : RoomDatabase() {

    abstract val eventDao: EventDao

}
