package com.example.sporteventtracker.model.models

import kotlinx.serialization.Serializable

interface FirebaseDocument<T> {
    val fields: T?
    val name: String?
    val createTime: String?
    val updateTime: String?
}

@Serializable
class EventFirebaseDocument(
    override val fields: WrappedEventFields,
    override val name: String? = null,
    override val createTime: String? = null,
    override val updateTime: String? = null
) : FirebaseDocument<WrappedEventFields>
