package com.example.sporteventtracker.model.persistence.event

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events")
data class EventEntity(
    val name: String,
    val location: String,
    val timeEnd: Long,
    val timeStart: Long,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
)
