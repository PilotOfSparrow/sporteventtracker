package com.example.sporteventtracker.model.api

import com.example.sporteventtracker.BuildConfig
import com.example.sporteventtracker.model.models.EventFirebaseDocument
import com.example.sporteventtracker.model.models.EventsFirebaseDocumentList
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface EventApi {

    @GET("events?key=${BuildConfig.FIREBASE_API_KEY}")
    fun getEvents(): Single<EventsFirebaseDocumentList>

    @POST("events?key=${BuildConfig.FIREBASE_API_KEY}")
    fun saveEvent(@Body eventFields: EventFirebaseDocument): Single<EventFirebaseDocument>

}
