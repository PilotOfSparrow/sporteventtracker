package com.example.sporteventtracker.model.models

import kotlinx.serialization.Serializable

@Serializable
sealed class WrappedValue<T> {
    @Serializable
    data class StringValue(val stringValue: String) : WrappedValue<String>()

    @Serializable
    data class IntegerValue(val integerValue: Long) : WrappedValue<Long>()
}
