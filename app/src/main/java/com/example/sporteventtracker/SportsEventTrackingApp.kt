package com.example.sporteventtracker

import android.app.Application
import com.example.sporteventtracker.di.ComponentsHolder
import com.example.sporteventtracker.di.viewmodel.ViewModelFactory
import javax.inject.Inject

class SportsEventTrackingApp : Application() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    init {
        ComponentsHolder.initialize(this)
    }

}
