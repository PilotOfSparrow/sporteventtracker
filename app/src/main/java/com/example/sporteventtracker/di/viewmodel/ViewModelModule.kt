package com.example.sporteventtracker.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sporteventtracker.viewmodel.AddEventViewModel
import com.example.sporteventtracker.viewmodel.ShowEventsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(AddEventViewModel::class)
    abstract fun bindAddEventViewModel(viewModel: AddEventViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShowEventsViewModel::class)
    abstract fun bindShowEventsViewModel(viewModel: ShowEventsViewModel): ViewModel

}
