package com.example.sporteventtracker.di.app

import com.example.sporteventtracker.SportsEventTrackingApp
import com.example.sporteventtracker.di.viewmodel.ViewModelModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        NetworkModule::class,
        PersistenceModule::class,
        ViewModelModule::class
    ]
)
interface ApplicationComponent {

    fun inject(app: SportsEventTrackingApp)

}
