package com.example.sporteventtracker.di

import com.example.sporteventtracker.SportsEventTrackingApp
import com.example.sporteventtracker.di.app.ApplicationComponent
import com.example.sporteventtracker.di.app.ApplicationModule
import com.example.sporteventtracker.di.app.DaggerApplicationComponent

object ComponentsHolder {

    lateinit var appComponent: ApplicationComponent
        private set

    fun initialize(app: SportsEventTrackingApp) {
        appComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(app))
            .build()
            .apply { inject(app) }
    }

}
