package com.example.sporteventtracker.di.app

import android.content.Context
import androidx.room.Room
import com.example.sporteventtracker.model.persistence.Database
import com.example.sporteventtracker.model.persistence.event.EventDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PersistenceModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): Database = Room
        .databaseBuilder(context, Database::class.java, "database")
        .build()

    @Provides
    fun provideEventDao(database: Database): EventDao = database.eventDao

}
