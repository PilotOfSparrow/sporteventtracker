package com.example.sporteventtracker.di.app

import com.example.sporteventtracker.BuildConfig
import com.example.sporteventtracker.model.api.EventApi
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        private const val REQUEST_TIMEOUT_SEC = 40L

        private val DEFAULT_CONTENT_TYPE = MediaType.get("application/json")
        private val CALL_ADAPTER_FACTORY =
            RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())
    }

    @Provides
    @Singleton
    fun provideEventApi(retrofit: Retrofit): EventApi = retrofit.create(EventApi::class.java)

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit = buildRetrofit(
        client = client,
        baseUrl = BuildConfig.BASE_URL
    )

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = buildClient()

    private fun buildRetrofit(
        client: OkHttpClient,
        baseUrl: String,
        callAdapter: CallAdapter.Factory = CALL_ADAPTER_FACTORY
    ): Retrofit = Retrofit
        .Builder()
        .client(client)
        .baseUrl(baseUrl)
        .addConverterFactory(Json.asConverterFactory(DEFAULT_CONTENT_TYPE))
        .addCallAdapterFactory(callAdapter)
        .build()

    private fun buildClient(): OkHttpClient = OkHttpClient.Builder()
        .readTimeout(REQUEST_TIMEOUT_SEC, TimeUnit.SECONDS)
        .writeTimeout(REQUEST_TIMEOUT_SEC, TimeUnit.SECONDS)
        .connectTimeout(REQUEST_TIMEOUT_SEC, TimeUnit.SECONDS)
        .apply {
            if (BuildConfig.DEBUG) {
                addNetworkInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }
        }
        .build()

}
