package com.example.sporteventtracker.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.repository.EventRepository
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ShowEventsViewModel @Inject constructor(
    private val eventRepository: EventRepository
) : ViewModel() {

    val eventsLiveData = MutableLiveData<List<EventFields>>()

    private var eventsLoadingDisposable: Disposable? = null

    init {
        fetchEvents()
    }

    override fun onCleared() {
        eventsLoadingDisposable?.dispose()
        super.onCleared()
    }

    fun fetchEvents() {
        eventsLoadingDisposable = eventRepository
            .getEvents()
            .subscribe { events, throwable ->
                eventsLiveData.postValue(events.orEmpty().sortedBy(EventFields::timeStart))
            }
    }

}
