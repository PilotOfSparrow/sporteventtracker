package com.example.sporteventtracker.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sporteventtracker.model.models.EventFields
import com.example.sporteventtracker.model.repository.EventRepository
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class AddEventViewModel @Inject constructor(
    private val eventRepository: EventRepository
) : ViewModel() {

    val savingResultLiveData = MutableLiveData<Boolean>()

    private var savingDisposable: Disposable? = null

    override fun onCleared() {
        savingDisposable?.dispose()
        super.onCleared()
    }

    fun saveEvent(eventFields: EventFields) {
        savingDisposable =
            eventRepository
                .saveEvent(eventFields)
                .subscribe { result, _ ->
                    savingResultLiveData.postValue(result ?: false)
                }
    }

}
